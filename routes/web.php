<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/panel', 'RedirectController@index');
Route::get('/panel/add', 'AddRedirectController@index');
Route::post('/panel/add', 'AddRedirectController@submit');
Route::get('/panel/edit/{id}','AddRedirectController@indexEdit');
Route::post('/panel/edit/{id}','AddRedirectController@submitEdit');
Route::get('/redirectme','RedirectmeController@redirectme');