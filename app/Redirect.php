<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Redirect extends Model
{
    protected $table = 'redirection';

    protected $id;

    protected $from;

    protected $to;

    protected $note;

    protected $code;

    protected $activated;

    protected $fillable = ['from','to','note','code','id', 'activated'];

    public function __construct(array $attributes = [])
    {

        $datas = [
            'from' => 'http://127.0.0.1:8000/redirectme',
            'to' => 'https://www.google.com',
            'note' => 'blablanote',
            'code' => '302',
            'id' => '1'
        ];
        parent::__construct($datas);
    }
}
