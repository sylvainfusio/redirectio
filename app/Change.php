<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Change extends Model
{
    protected $table = 'RedirectionChanges';

    protected $id_changes;

    protected $old_origin;

    protected $old_destination;

    protected $changes_note;

    protected $user;

    protected $created_at;

    protected $id_origin;

    protected $old_status_code;

    protected $fillable = ['old_origin','old_destination','changes_note',
        'user','created_at','old_status_code','id_origin'];

    public function __construct(array $attributes = [])
    {

        $this->attributes['created_at'] = Carbon::createFromFormat('d/m/Y h:i:s', '06/06/666 06:06:06');
        $datas = [
            'id_origin' => '1',
            'old_origin' => 'old_origin.com',
            'old_destination' => 'old_destination.com',
            'changes_note' => 'blabla changes note',
            'user' => 'Sylvain',
            'old_status_code' => '666',
        ];
        parent::__construct($datas);
    }
}
