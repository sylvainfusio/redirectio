<?php

namespace App\Http\Controllers;
use App\Redirect;
use Illuminate\Http\Request;
use function Sodium\add;

class RedirectController extends Controller
{
    protected $from;
    protected $to;
    function __construct() {
        //$this->middleware('auth');
    }

    function index() {

        $redirections = $this->getRedirectList();

        return response() -> view("redirectPanel", [ 'redirections' => $redirections] );
    }


    function getRedirectList(){

        //search in database the list of redirection ordered by date (updated or created?)

        $redirectList = [];
        array_push($redirectList, new Redirect());

        return $redirectList;
    }


}
