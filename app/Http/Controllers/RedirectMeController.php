<?php

namespace App\Http\Controllers;

use App\Redirect;
use Illuminate\Http\Request;

class RedirectMeController extends Controller
{
    protected $redirectObject;
    private $linkToRedirect = 'https://www.fusio.net';
    private $code = 302;

    function redirectme(){
        $currenturl = url()->current();
        $this->getDestinationFromDatabase($currenturl);
        return redirect($this->linkToRedirect, $this->code);
    }

    function getDestinationFromDatabase($from){
        //search in database the correspondent destination

        $this->redirectObject = new Redirect();

        if($from === $this->redirectObject->__get('from')){
            $this->linkToRedirect = $this->redirectObject->__get('to');
            $this->code = $this->redirectObject->__get('code');
        }
    }
}
