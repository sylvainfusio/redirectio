<?php

namespace App\Http\Controllers;

use App\Redirect;
use App\Change;
use Illuminate\Http\Request;

class AddRedirectController extends Controller
{
    //

    public function __construct()
    {
        //$this->middleware('auth');
    }

    function index() {
        return response() -> view("add");
    }

    function submit(Request $request) {
        //insert the new redirection in database
        $datas = [
            'hasBeenAdded' => true,
            'from'         => $request->all()['from'],
            'to'           => $request->all()['to']
        ];

        $request->validate([
            'from' => 'required|max:300|min:3',
            'to' => 'required|max:300|min:3',
            'code' => 'required|in:301,302,303,307,308',
            'activated' => 'required|in:true,false',
            'note' => 'nullable|max:500',
        ]);
        return response() -> redirectTo('/panel');
    }

    function indexEdit($id) {
        //lets search from database with $id

        $datas = [
            'redirect' => new Redirect(),
            'changes' => [new Change()],
        ];

        return response() -> view("add", $datas);
    }

    function submitEdit($id,Request $request) {
        //update the redirection in database with correspondent $id
        $datas = [
            'hasBeenAdded' => true,
            'from'         => $request->all()['from'],
            'to'           => $request->all()['to']
        ];

        $request->validate([
            'from' => 'required|max:300|min:3',
            'to' => 'required|max:300|min:3',
            'code' => 'required|in:301,302,303,307,308',
            'activated' => 'required|in:true,false',
            'note' => 'nullable|max:500',
        ]);
        return response() -> view("redirectPanel", $datas);
    }
}
