@extends('layouts.app')

@section('customstyles')

@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">Panel</div>
                    <div class="card-body">
                        <a class="btn btn-success" href="/panel/add" style="margin-bottom : 15px" role="button">Add redirection</a>
                        <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th scope="col">Origin Link</th>
                                <th scope="col">Destination Link</th>
                                <th scope="col">Status Code</th>
                                <th scope="col">Actions</th>
                            </tr>

                            <tbody>
                            @foreach($redirections as $redirection)
                                <tr>
                                    <td>{{$redirection->from}}</td>
                                    <td>{{$redirection->to}}</td>
                                    <td>{{$redirection->code}}</td>
                                    <td><a class="btn btn-primary" href="/panel/edit/{{$redirection->id}}" role="button">Edit</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection
