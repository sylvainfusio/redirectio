
<div class="col-md-12">

    <div class="card">
    <div class="card-header">
        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#history" aria-expanded="false" aria-controls="history">
            History
        </button>
    </div>
    <div class="collapse" id="history">
        <div class="card-body">
            <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th scope="col">Old Origin</th>
                    <th scope="col">Old Destination</th>
                    <th scope="col">Old Status Code</th>
                    <th scope="col">Notes</th>
                    <th scope="col">User</th>
                    <th scope="col">Date</th>
                </tr>

                <tbody>
                @foreach($changes as $change)
                    <tr>
                        <td>{{$change->old_origin}}</td>
                        <td>{{$change->old_destination}}</td>
                        <td>{{$change->old_status_code}}</td>
                        <td>{{$change->changes_note}}</td>
                        <td>{{$change->user}}</td>
                        <td>{{$change->created_at}}</td>

                     </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div></div>
</div>
