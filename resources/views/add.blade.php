@extends('layouts.app')

@section('customstyles')

@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">{{ isset($redirect->id) ? 'Edit' : 'Add'}}</div>

                    <div class="card-body">
                        <form action="/panel/add" ID="" name="" method="post"
                              data-show-errors="true">
                            {{ csrf_field() }}

                            {!! isset($redirect->id) ? '<input type="hidden" id="custId" name="id" value="'.$redirect->id.'">' : ''!!}

                            <div class="form-group row">
                                <label class="col-md-5 col-sm-12 col-form-label">
                                    Redirect from : <em>*</em>
                                </label>
                                <div class="col-md-7 col-sm-12">

                                    <input type="text" class="form-control input-lg"
                                           required
                                           pattern="^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$"
                                           name="from" value="{{ isset($redirect->from) ? $redirect->from : old('from')}}"
                                           placeholder="RedirectFrom.com"
                                    />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-5 col-sm-12 col-form-label">
                                    Redirect to : <em>*</em>
                                </label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" class="form-control input-lg"
                                           required
                                           pattern="^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$"
                                           name="to" value="{{ isset($redirect->to) ? $redirect->to : old('to')}}"
                                           placeholder="RedirectTo.com"
                                    />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-5 col-sm-12 col-form-label">
                                    Status Code : <em>*</em>
                                </label>
                                <div class="col-md-7 col-sm-12">
                                    <select name="code">
                                        <option value="301" {{ old('code') == 301 ? 'selected' : '' }}>301</option>
                                        <option value="302" {{ old('code') == 302 ? 'selected' : '' }}>302</option>
                                        <option value="303" {{ old('code') == 303 ? 'selected' : '' }}>303</option>
                                        <option value="307" {{ old('code') == 307 ? 'selected' : '' }}>307</option>
                                        <option value="308" {{ old('code') == 308 ? 'selected' : '' }}>308</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-5 col-sm-12 col-check-label" for="activated"> Activated <em>*</em></label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="checkbox"
                                           name="activated" value="false"
                                           id="activated"
                                            {!! old("activated") ? 'checked="checked"' : '' !!}
                                            {{ isset($redirect->activated) && $redirect->activated == true ? 'checked' : ''}}
                                    >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-5 col-sm-12" for="note"> Note </label>
                                <div class="col-md-7 col-sm-12">
                                    <textarea class="form-control"
                                           name="note"
                                           id="note"

                                    >{{ isset($redirect->activated) && $redirect->activated == true ? 'checked' : ''}}</textarea>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary ">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            @if(isset($redirect->id))
                @include('blocks.history')
                @endif
        </div>
    </div>
@endsection

@section('js')

@endsection
