<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
class AddRedirectTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    public function test_add_redirect_view()
    {
        $user = new User();
        $user->id = 1;
        $this->be($user);
        $response = $this->call('GET', '/panel/add');
        $this->assertEquals(200, $response->status());
    }
    public function test_add_redirection()
    {
        $this->withoutMiddleware();
        $postData = [
            "from"      => "from.com",
            "to"        => "to.com",
            "activated" => true,
            "code" => "301",

        ];
        $response = $this->call("POST", '/panel/add', $postData);
        $this->assertEquals(302, $response->status());
    }
    public function test_edit_redirection_view()
    {
        $response = $this->call("GET", '/panel/edit/1');
        $this->assertEquals(200, $response->status());
    }

    public function test_edit_redirect_submit()
    {
        $this->withoutMiddleware();
        $user = new User();
        $user->id = 1;
        $this->be($user);

        $postData = [
            "from"      => "from.com",
            "to"        => "to.com",
            "activated" => true,
            "code"      => 301,
        ];

        $response = $this->call('POST', '/panel/edit/1', $postData);
        $this->assertEquals(302, $response->status());
    }

}
