<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
class RedirectTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRedirect()
    {
        $user = new User();
        $user->id = 1;
        $this->be($user);

        $response = $this->call('GET', 'panel');
        $this->assertEquals(200, $response->status());
    }

}
