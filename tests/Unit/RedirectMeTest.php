<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Redirect;
use Mockery as m;
class RedirectMeTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function test_if_we_are_redirected()
    {
        $this->app['session']->setPreviousUrl('http://127.0.0.1:8000/redirectme');
        $response = $this->get('http://127.0.0.1:8000/redirectme')->assertRedirect('https://www.google.com');
        $this->assertEquals(302, $response->status());
    }
}
